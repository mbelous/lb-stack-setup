from celery import Celery
from catboost import CatBoostClassifier

app = Celery('tasks', broker='amqp://guest:guest@rabbitmq:5672//')

@app.task
def add(a, b):
    return a + b

@app.task
def fibonacci_number(n):
    if n <= 1:
        return 1
    result = [
        fibonacci_number.delay(n - 1).get(timeout = n - 1),
        fibonacci_number.delay(n - 2).get(timeout = n - 2)]
    return result[0] + result[1]

@app.task
def apply_catboost_classifier(model_file, test_data):
    model = CatBoostClassifier()
    model.load_model(model_file) 
    return model.predict(test_data)
