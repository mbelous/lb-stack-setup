from tasks import add, fibonacci_number, apply_catboost_classifier
from catboost import CatBoostClassifier

train_data = [[1, 3],
              [100, 100],
              [125, 125],
              [300, 300],
              [500, 500],
              [1, 7]]
train_labels = [0, 0, 1, 0, 0, 0]
model = CatBoostClassifier(learning_rate=0.03)
model.fit(train_data,
          train_labels,
          verbose=False)
model.save_model("models/higgs.cbm")

result_add = add.delay(2, 3)
result_fibonacci = fibonacci_number.delay(5)
result_catboost = apply_catboost_classifier.delay("models/higgs.cbm", [[-273.0, 125], [0, 0], [1, 1], [128, 128], [300, 300]])
print(result_add.get())
print(result_fibonacci.get())
print(result_catboost.get())
